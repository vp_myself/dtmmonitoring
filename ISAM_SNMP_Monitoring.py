from dtmon.dtSNMPHost import DT_SNMPHost

def main():
	dtendpoint = {
		"url": <DT_URL>,
		"apiToken": <DT_TOKEN>
	}
	
	isamEndpoint = {
		"host": "10.0.0.1",
		"port": 161,
		"snmpAuth": {
			"version": 2,
			"username": "public",
			"authKey": "testsnmp",
			"privKey": "testsnmp",
			"authProtocol": "md5",
			"privProtocol": "des"
		}
	}
	
	deviceDisplay = {
		"id": "f5.test1", 
		"name": "F5 Test 1", 
		"type": "F5 Big IP",
#		"ips":["10.0.0.1"], 
		"ports":["161"], 
#		"configConsoleUrl":"",
		"tags": ["F5 Big IP", "F5"],
		"icon":"http://assets.dynatrace.com/global/icons/infographic_rack.png"
	}
	
	#logDetails = {"level":"info", "location": "/tmp/DTDPowerESMon.log"}
	isam1 = DT_SNMPHost(dtendpoint, isamEndpoint, deviceDisplay)
	isam1.dtrun()
	
	return
	
if __name__ == '__main__':
  main()		