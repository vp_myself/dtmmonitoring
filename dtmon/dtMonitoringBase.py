import requests
import time
import datetime
import sched
import random
import re
import ast
import logging
from pymemcache.client import base


class DT_MonitoringBase(object):
    dtEndpoint, deviceAuth, deviceDisplay = {}, {}, {}
    logFormat = ""
    logDetails, timeout = {}, {}
    client = base.Client(('localhost', 11211))

    def __init__(
            self,
            dtEndpoint,
            deviceAuth,
            deviceDisplay,
            timeout={"dtserver": 10, "device": 10},
            logDetails={"level": "error",
                        "location": "/tmp/DTMonitoring.log"}):
        self.timeout = timeout
        self.dtEndpoint = dtEndpoint
        self.deviceAuth = deviceAuth
        self.deviceDisplay = deviceDisplay
        self.logDetails = logDetails
        self.__validateInput()
        self.logFormat = '%(asctime)-15s - %(levelname)s - %(message)s'
        logging.basicConfig(format=self.logFormat,
                            filename=self.logDetails["location"],
                            level=self.__getLogLevel())

    def __validateInput(self):
        if (not isinstance(self.dtEndpoint, dict)
                or not isinstance(self.deviceAuth, dict)
                or not isinstance(self.deviceDisplay, dict)
                or not isinstance(self.timeout, dict)
                or not isinstance(self.logDetails, dict)):
            raise TypeError('dtEndpoint, deviceAuth, deviceDisplay, timeout'
                            ' and logDetails should be of type dict')

        if (not "url" in self.dtEndpoint
            or self.dtEndpoint["url"].strip() == ""
            or not "apiToken" in self.dtEndpoint
                or self.dtEndpoint["apiToken"].strip() == ""):
            raise ValueError("url and apiToken of dtEndpoint"
                             " should not be empty")

        if (not "host" in self.deviceAuth
                or self.deviceAuth["host"].strip() == ""):
            raise ValueError("host should not be empty")

        if (not "dtserver" in self.timeout
            or not isinstance(self.timeout["dtserver"], int)
            or not "device" in self.timeout
                or not isinstance(self.timeout["device"], int)):
            raise ValueError("dtserver and device timeout"
                             " should be of type int")

        if (not "level" in self.logDetails
            or self.logDetails["level"].strip() == ""
            or not "location" in self.logDetails
                or self.logDetails["location"].strip() == ""):
            raise ValueError("level and location of log should not be empty")

        if (not "tags" in self.deviceDisplay
                or not isinstance(self.deviceDisplay["tags"], list)):
            self.deviceDisplay["tags"] = []

    def __getLogLevel(self):
        lowerCaseLevel = self.logDetails["level"].lower()
        if(lowerCaseLevel == "error"):
            return logging.ERROR
        elif(lowerCaseLevel == "warning"):
            return logging.WARNING
        elif(lowerCaseLevel == "info"):
            return logging.INFO
        elif(lowerCaseLevel == "debug"):
            return logging.DEBUG

    def __tickToString(self, tfield):
        ticks = int(tfield)
        seconds = ticks/100
        tfield = str(datetime.timedelta(seconds=seconds))
        return tfield

    def _registerDTMetric(self, tsId, json={}, verify=None, timeout=None):
        url = ('%s/api/v1/timeseries/%s?Api-Token=%s' % (
            self.dtEndpoint["url"], tsId, self.dtEndpoint["apiToken"]))
        logging.debug("Registering metric url: %s", url)
        r = self._makeRequest("put",
                              url,
                              json=json,
                              verify=verify,
                              timeout=timeout)

    def _sendMetricData(self, deviceId, json={}, verify=None, timeout=None):
        url = ('%s/api/v1/entity/infrastructure/custom/%s?Api-Token=%s' % (
            self.dtEndpoint["url"], deviceId, self.dtEndpoint["apiToken"]))
        logging.debug("Sending metrics url: %s", url)
        r = self._makeRequest("post",
                              url,
                              json=json,
                              verify=verify,
                              timeout=timeout)

    def _makeRequest(self,
                     method,
                     url,
                     headers={},
                     json={},
                     verify=None,
                     timeout=None):
        _verify = False
        if verify is not None:
            _verify = verify
        _timeout = self.timeout["device"]
        if timeout is not None:
            _timeout = timeout
        try:
            if method.lower() == "get":
                return requests.get(
                    url,
                    json=json,
                    verify=_verify,
                    timeout=_timeout,
                    headers=headers)
            elif method.lower() == "post":
                return requests.post(
                    url,
                    json=json,
                    verify=_verify,
                    timeout=_timeout,
                    headers=headers)
            elif method.lower() == "put":
                return requests.put(
                    url,
                    json=json,
                    verify=_verify,
                    timeout=_timeout,
                    headers=headers)
            elif method.lower() == "delete":
                return requests.delete(
                    url,
                    json=json,
                    verify=_verify,
                    timeout=_timeout,
                    headers=headers)
        except requests.exceptions.Timeout:
            logging.error('Request to %s Timed Out, exceeding %d seconds',
                          url, _timeout)
        except requests.exceptions.RequestException as e:
            logging.error("Config: Request to %s has exception %s",
                          url, e)
        return None

    def _getNestedValue(self, dict, list):
        d = dict
        for item in list:
            d = d[item]
        return d

    def _findByKey(self, collection, key, matchCondition=None, match=True):
        result = None
        if(isinstance(collection, list)):
            result = filter(lambda x:
                            (x[key] == matchCondition) == match,
                            collection)
        elif(isinstance(collection, dict)):
            if(matchCondition is None):
                result = filter(lambda kv:
                                kv[0] == key,
                                collection.iteritems())
            else:
                result = filter(lambda kv:
                                (kv[1] == matchCondition) == match,
                                collection.iteritems())
        return result

    #If Metric is the counter or keeps increasing since up time
    def _getCurrentMetric(self, mkey, thisPull, resetAt=None):
        cachedKey = self.deviceDisplay["id"] + "_" + mkey
        previousMetric = self.client.get(cachedKey)
        currentMetric = 0.0
        if previousMetric is not None:
            temp = thisPull - float(previousMetric)
            if temp < 0:
                if resetAt is not None:
                    currentMetric = temp + resetAt
                else:
                    currentMetric = float(previousMetric)
            else:
                currentMetric = float(temp)
        self.client.set(cachedKey, thisPull)
        return currentMetric
