from .dtMonitoringSNMP import DT_SNMPMonitoring
from pysnmp.entity.rfc3413.oneliner import cmdgen
from pysnmp.hlapi import *
import requests, time, datetime, sched, random
import re
import logging

class DT_SNMPHost(DT_SNMPMonitoring):
	propsUmbrella = '1.3.6.1.2.1.1';
	propsMap  = [
		{"properyName" : "Up time", "detailedOid":".3.0"},
		{"properyName" : "System Name", "detailedOid":".5.0"},
		{"properyName" : "Data Center", "detailedOid":".6.0"}
	];
	
	memUmbrella = "1.3.6.1.2.1.25.2.3.1";
	memMap = [
		{ "timeseriesId" : "custom:custom_package.memory.physical.size", "dimensions":[], "displayName" : "Total Physical Memory", "unit" : "Kibibyte", "detailedOid" : '.5.1'},
		{ "timeseriesId" : "custom:custom_package.memory.physical.usage", "dimensions":[], "displayName" : "Used Physical Memory", "unit" : "Kibibyte", "detailedOid" : '.6.1'},
	]
	
	networkUmbrella = {"umbrella": "1.3.6.1.2.1.2.2.1", "query": ["1.3.6.1.2.1.2.2.1.10", "1.3.6.1.2.1.2.2.1.14", "1.3.6.1.2.1.2.2.1.16", "1.3.6.1.2.1.2.2.1.20"]}	
	networkMap = [
		{ "timeseriesId" : "custom:custom_package.network.traffic", "detail": [{"oid": ".10", "type": "incoming"}, {"oid": ".16", "type": "outgoing"}], "dimensions":["type"], "displayName" : "Network Traffic", "unit" : "Byte"},
		{ "timeseriesId" : "custom:custom_package.network.error", "detail": [{"oid": ".14", "type": "incoming"}, {"oid": ".20", "type": "outgoing"}], "dimensions":["type"], "displayName" : "Network Errors", "unit" : "Count"}
	]
	
	cpuTypeUmbrella = "1.3.6.1.2.1.25.4.2.1.6";
	cpuTypeMap = [
		{"cat": 2, "list": []},
		{"cat": 4, "list": []}
	]
	
	cpuTimeUmbrella = "1.3.6.1.2.1.25.5.1.1.1";
	cpuTimeDetailed = [
		{ "timeseriesId" : "custom:custom_package.cpuTime", "dimensions":["type"], "detail": [{"cat": 2, "type": "System"}, {"cat": 4, "type": "Application"}], "displayName" : "CPU Time", "unit" : "Centisecond"}
	]

	def __init__(self, dtEndpoint, deviceAuth, deviceDisplay, timeout={"dtserver": 10, "device": 10}, \
		logDetails={"level":"error", "location": "/tmp/dtISAMSNMPMon.log"}):
		super(DT_SNMPHost, self).__init__(dtEndpoint, deviceAuth, deviceDisplay, timeout=timeout, logDetails=logDetails)

	def dtrun(self):
		#Get Props
		iter = self._snmpBulkConnect(self.propsUmbrella);
		self._processIterBulk(iter, self.propsUmbrella, self.propsMap, self.__setValueCallback)
		propsDict = {}
		for item in self.propsMap:
			propsDict[item["properyName"]] = item["value"]
		
		memUtilisation = [{ "timeseriesId" : "custom:custom_package.memory.physical.utilisation", "dimensions":[], "displayName" : "Memory Utilisation", "unit" : "Kibibyte"}]
		iter = self._snmpBulkConnect(self.memUmbrella);
		self._processIterBulk(iter, self.memUmbrella, self.memMap, self.__setValueCallback)		
		memUtilisation[0]["value"] = 100*float(self.memMap[1]["value"])/float(self.memMap[0]["value"])
		memUtilisation[0]["unit"] = "Percent"
		
		iter = self._snmpBulkConnect(self.networkUmbrella["query"]);
		self._processIterBulk(iter, self.networkUmbrella["umbrella"], self.networkMap, self.__sumValueCallback)
		
		for item in self.networkMap:
			if("detailedOid" in item):
				current = self._getCurrentMetric(item["timeseriesId"], float(item["value"]), resetAt=4294967296.0)
				item["value"] = current
			else:
				for it in item["detail"]:
					current = self._getCurrentMetric(item["timeseriesId"]+ it["type"], float(it["value"]), resetAt=4294967296.0)
					it["value"] = current
	
		iter = self._snmpBulkConnect(self.cpuTypeUmbrella);
		self._processIterBulk(iter, self.cpuTypeUmbrella, self.cpuTypeMap, self.__categorizeOid)
		
		iter = self._snmpBulkConnect(self.cpuTimeUmbrella);
		self._processIterBulk(iter, self.cpuTimeUmbrella, self.cpuTimeDetailed, self.__sumValueBasedOnCPUType)
		
		cpuTimePrefix = self.cpuTimeDetailed[0]["timeseriesId"]
		for item in self.cpuTimeDetailed[0]["detail"]:
			if("value" in item):
				current = self._getCurrentMetric(cpuTimePrefix, item["value"], resetAt=4294967296.0)
				item["value"] = current * 10
			else:
				item["value"] = 0.0
		self.cpuTimeDetailed[0]["unit"] = "Millisecond"	
		metricArr = memUtilisation + self.networkMap + self.cpuTimeDetailed
		self.__registerMetrics(metricArr)
		self.__sendMetrics(propsDict, metricArr)
		
		
	def __setValueCallback(self, varBinds, umbrella, mapping):
		u_oid = umbrella
		for oid, value in varBinds:
			ix, iy = None, None
			for index, val in enumerate(mapping):
				if "detailedOid" in val:
					if u_oid+val["detailedOid"] == str(oid):
						mapping[index]["value"] = value.prettyPrint()
				else:
					for i, v in enumerate(val):
						if u_oid + v["oid"] == str(oid):
							ix = index
							iy = i
							break
					if ix is not None and iy is not None:
						mapping[ix]["detail"][iy]["value"] = value.prettyPrint()
						break	
	
	
	def __sumValueCallback(self, varBinds, umbrella, mapping):
		u_oid = umbrella
		for oid, value in varBinds:
			ix, iy = None, None
			for index, val in enumerate(mapping):
				if "detailedOid" in val:
					if str(oid).startswith(u_oid+val["detailedOid"]):
						if(not "value" in mapping[index]):
							mapping[index]["value"] = 0
						else:
							mapping[index]["value"] += float(value.prettyPrint())
				else:
					for i, v in enumerate(val["detail"]):
						if str(oid).startswith(u_oid+v["oid"]):
							ix = index
							iy = i
							break
					if ix is not None and iy is not None:
						mapping[ix]["detail"][iy]["value"] = value.prettyPrint()
						break
			

	
	def __categorizeOid(self, varBinds, umbrella, mapping):
		u_oid = umbrella
		for oid, value in varBinds:
			indices = [i for i, v in enumerate(mapping) if value == v["cat"]]
			if(len(indices) > 0):
				index = indices[0]
				m = re.search(umbrella + '([\.\d]*)', str(oid))
				if(m.group(1) is not None):
					self.cpuTypeMap[index]["list"].append(m.group(1))
					
	def __sumValueBasedOnCPUType(self, varBinds, umbrella, mapping):
		u_oid = umbrella
		for oid, value in varBinds:
			ix, iy = None, None
			m = re.search(umbrella + '([\.\d]*)', str(oid))
			for index, val in enumerate(self.cpuTypeMap):
				if (m is not None and m.group(1) in val["list"]):
					if(not "value" in mapping[0]["detail"][index]):
						mapping[0]["detail"][index]["value"] = 0
					else:
						mapping[0]["detail"][index]["value"] += float(value.prettyPrint())
	
	def __registerMetrics(self, metricArr):
		for metric in metricArr:
			data = {"displayName": metric["displayName"], "unit": metric["unit"], "dimensions": metric["dimensions"], "types": [self.deviceDisplay["type"]]}
			logging.debug('metric data: %s', data)
			self._registerDTMetric(metric['timeseriesId'], json=data)
			
	def __sendMetrics(self, propsDict, metricArr):
		data = []
		for item in metricArr:
			if not "detail" in item:
				_item = {"timeseriesId": item["timeseriesId"], "dimensions": {}, "dataPoints": [ [int(time.time() * 1000), item["value"]] ]}
				data.append(_item)
			else:
				for it in item["detail"]:
					dimName = item["dimensions"][0]
					_item = {"timeseriesId": item["timeseriesId"], "dimensions": {dimName: it[dimName]}, "dataPoints": [ [int(time.time() * 1000), it["value"]] ]}
					data.append(_item)
		device = {
				"displayName": self.deviceDisplay["name"], 
				"type": self.deviceDisplay["type"], 
				"tags": self.deviceDisplay["tags"], 
				"properties": propsDict,
				"favicon" : self.deviceDisplay["icon"],
				"series": data
			}
		print device	
		self._sendMetricData(self.deviceDisplay["id"], json=device)
