from .dtMonitoringBase import DT_MonitoringBase
from pysnmp.entity.rfc3413.oneliner import cmdgen
from pysnmp.hlapi import *
import requests
import time
import datetime
import sched
import random
import re
import logging


class DT_SNMPMonitoring(DT_MonitoringBase):
    authProtoDict = {
        'md5': usmHMACMD5AuthProtocol,
        'sha': usmHMACSHAAuthProtocol,
        'sha224': usmHMAC128SHA224AuthProtocol,
        'sha256': usmHMAC192SHA256AuthProtocol,
        'sha384': usmHMAC256SHA384AuthProtocol,
        'sha512': usmHMAC384SHA512AuthProtocol,
        'noauth': usmNoAuthProtocol,
    }

    privProtoDict = {
        'des': usmDESPrivProtocol,
        '3des': usm3DESEDEPrivProtocol,
        'aes': usmAesCfb128Protocol,
        'aes192': usmAesCfb192Protocol,
        'aes256': usmAesCfb256Protocol,
        'nopriv': usmNoPrivProtocol,
    }
    snmpAuthDetails = object()

    def __init__(self,
                 dtEndpoint,
                 deviceAuth,
                 deviceDisplay,
                 timeout={"dtserver": 10,
                          "device": 10},
                 logDetails={"level": "error",
                             "location": "/tmp/DTSNMPMonitoring.log"}):
        super(DT_SNMPMonitoring, self).__init__(dtEndpoint,
                                                deviceAuth,
                                                deviceDisplay,
                                                timeout=timeout,
                                                logDetails=logDetails)
        self.__validateInput()
        self.__buildAuthDetails()

    def __validateInput(self):
        authVersionReq = ["authKey", "privKey", "authProtocol", "privProtocol"]
        if(not "snmpAuth" in self.deviceAuth):
            raise ValueError("No auth details for this SNMP Device")
        snmpAuth = self.deviceAuth["snmpAuth"]
        if (not "username" in snmpAuth
            or snmpAuth["username"].strip() == ""):
            raise ValueError('snmpAuth["username"] should not be empty')
        if (not "version" in snmpAuth
            or not isinstance(snmpAuth["version"], int)):
            raise ValueError('snmpAuth["version"] should not be empty'
                             ' and should be an integer')
        snmpAuthKeysSet = set(snmpAuth.keys())
        if(snmpAuth["version"] == 3):
            if(not set(authVersionReq).issubset(snmpAuthKeysSet)):
                raise ValueError(
                    "%s of auth should not be none if v3 is used" % (str(authVersionReq)))
            authProtoKeys = self.authProtoDict.keys()
            privProtoKeys = self.privProtoDict.keys()
            if(not snmpAuth["authProtocol"] in authProtoKeys):
                raise ValueError("snmpAuth['authProtocol'] should only has the following values: %s" % (
                    str(authProtoKeys)))
            if(not snmpAuth["privProtocol"] in privProtoKeys):
                raise ValueError("snmpAuth['privProtocol'] should only has the following values: %s" % (
                    str(privProtoKeys)))

    def __buildAuthDetails(self):
        snmpAuth = self.deviceAuth["snmpAuth"]
        if(snmpAuth["version"] == 3):
            snmpAuthDetails = UsmUserData(
                snmpAuth["username"],
                snmpAuth["authKey"],
                snmpAuth["privKey"],
                snmpAuth["authProtocol"],
                snmpAuth["privProtocol"]
            )
        elif(snmpAuth["version"] == 2):
            self.snmpAuthDetails = CommunityData(snmpAuth["username"],
                                                 mpModel=0)

    def __snmpConnect(self, oid):
        iter = object()
        iter = getCmd(SnmpEngine(),
            self.snmpAuthDetails,
            UdpTransportTarget((self.deviceAuth["host"],
            self.deviceAuth["port"])),
            ContextData(),
            ObjectType(ObjectIdentity(oid))
        )
        next(iter)
        return iter

    def _snmpBulkConnect(self, oids):
        iter = object()
        if(isinstance(oids, str)):
            oidTypes = [ObjectType(ObjectIdentity(oids))]
        elif(isinstance(oids, list)):
            oidTypes = [ObjectType(ObjectIdentity(oid)) for oid in oids]
        iter = cmdgen.bulkCmd(
            SnmpEngine(),
            self.snmpAuthDetails,
            UdpTransportTarget((self.deviceAuth["host"],
            self.deviceAuth["port"])),
            ContextData(),
            0,25,
            *oidTypes,
            lexicographicMode=False)
        return iter

    def _processIterBulk(self, iter, umbrella, detailedMap, callback):
        t_iter = list(iter)
        for i in range(0, len(t_iter)):
            loopDone = False
            errorIndication, errorStatus, errorIndex, varBinds = t_iter[i]
            if errorIndication:
                logging.error('Error: %s',
                              errorIndication,
                              extra=self.logDetails)
            elif errorStatus:
                logging.error('Error: %s at %s', errorStatus.prettyPrint(),
                errorIndex and varBinds[int(errorIndex)-1][0] or '?',
                extra=self.logDetails)
            else:
                #success, emit the value to the log
                if callable(callback):
                    callback(varBinds, umbrella, detailedMap)
